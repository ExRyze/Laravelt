@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    {{ __('Blogs') }}
                    <a class="btn btn-danger" href="home">Home</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="btn btn-primary" href="blogs/create">Add Blog</a>

                </div>

                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>ID Blog</th>
                            <th>ID User</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Content</th>
                            <th class="text-center">Action</th>
                        </tr>
                    @foreach($blogs as $blog)
                        <tr>
                            <td>{{ $blog->id }}</td>
                            <td>{{ $blog->user_id }}</td>
                            <td>{{ $blog->title }}</td>
                            <td>{{ $blog->slug }}</td>
                            <td>{{ $blog->content }}</td>
                            <td class="text-center">
                                <a href="blogs/edit" class="btn bg-warning">Edit</a>
                                <a href="blogs/create" class="btn bg-danger">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

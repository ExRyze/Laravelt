@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
            <div class="card-header d-flex justify-content-between align-items-center mb-3">
                    {{ __('Edit Blog') }}
                    <a class="btn btn-danger" href="../blogs">Back</a>
                </div>
                <form action="{{ route('blogs.store') }}" method="post" class="col-sm-6 mb-3 m-auto">
                    @csrf
                    <div class="form-group d-flex flex-column">
                        <label for="title">Title</label>
                        <input type="text" name="title" id="title" class="rounded">
                    </div>
                    <div class="form-group d-flex flex-column">
                        <label for="slug">Slug</label>
                        <input type="text" name="slug" id="slug" class="rounded">
                    </div>
                    <div class="form-group d-flex flex-column">
                        <label for="content">Content</label>
                        <textarea name="content" id="content" cols="30" class="w-100 rounded"></textarea>
                    </div>
                    <div class="form-group d-flex flex-column">
                        <button type="submit" class="mt-3 btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
